using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerData", menuName = "PlayerData/New Playerdata")]
public class PlayerDataSOJ : ScriptableObject
{
    public float health;
    public float maxHealth;
    //public float armor (armadura�?, vida azul)
    public float speed;
    public BaseWeapon[] weapons; 
}
