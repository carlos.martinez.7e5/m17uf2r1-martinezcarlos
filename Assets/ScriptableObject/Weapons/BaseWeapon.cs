using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName = "Weapon/New Weapon")]
public class BaseWeapon : ScriptableObject
{
    [Header("Weapon Attributes")]
    [SerializeField] public string weaponName;
    [SerializeField] public Sprite sprite;
    [SerializeField] public int attack; 
    [SerializeField] public float bullets;
    [SerializeField] public float timeBetweenShoots;

    [Header("To Shoot")]
    [SerializeField] GameObject bullet;
    [SerializeField] private float bulletForce;
    private Camera camera;
    private Transform firePoint;
    
    private SpriteRenderer sr;
    private GameObject gun;
    
    private void OnEnable()
    {
        if(GameObject.Find("Main Camera")) camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        if (GameObject.Find("Gun"))
        {
            gun = GameObject.Find("Gun");
            firePoint = gun.GetComponent<Transform>();
            sr = gun.GetComponent<SpriteRenderer>();
        } 
    }

    public void Shoot()
    {
        var shoot = Instantiate(bullet, firePoint.transform.position, Quaternion.identity);
        shoot.transform.position = firePoint.position;
        shoot.GetComponent<Rigidbody2D>().velocity = firePoint.right * bulletForce;
    }

    public void GunRotation()
    {
        Vector3 mouse = camera.ScreenToWorldPoint(Input.mousePosition);
        mouse.z = 0;

        Vector3 lookAt = mouse - firePoint.position;
        firePoint.right = lookAt;
    }

    public void Reload()
    {
        //Recarga
    }
}


