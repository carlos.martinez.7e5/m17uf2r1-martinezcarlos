using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] public BaseWeapon weapon;
    SpriteRenderer sr;


    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        sr.sprite = weapon.sprite;
    }
}
