using UnityEngine;

//No se utiliza, sirve para un "maniqu�" que utilic� para hacer pruebas
public class Maniqui : MonoBehaviour
{
    public int vida;
    private Weapon _weapon;

    void Start()
    {
        if (vida < 20) vida = 20;
        _weapon = GameObject.Find("Gun").GetComponent<Weapon>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        vida -= _weapon.weapon.attack;
    }
}
