using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField] private int _points;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(this.gameObject);
        ScoreManager.Instance.Score += _points;
    }
}
