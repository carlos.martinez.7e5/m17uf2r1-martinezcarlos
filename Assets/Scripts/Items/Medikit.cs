using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Medikit : MonoBehaviour
{
    [SerializeField] private float _restoredHealth;
    [SerializeField] private PlayerDataSOJ _pd;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(this.gameObject);
        Health();
    }

    private void Health()
    {
        var aux = _pd.health;
        if (aux + _restoredHealth <= 100) _pd.health += _restoredHealth;
        else _pd.health = 100;
    }
}
