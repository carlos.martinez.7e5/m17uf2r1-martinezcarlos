using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDestruible
{
    void Break();

    void Drop();
    
}
