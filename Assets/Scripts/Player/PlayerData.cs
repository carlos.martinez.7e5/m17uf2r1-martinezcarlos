using UnityEngine;

//Ya no se utiliza, script de antes de utilizar scriptable object
public class PlayerData : MonoBehaviour
{
    public int health;
    
    public BaseWeapon[] weapons;
    
    private Weapon equipedWeapon;
    private int indexWeapon;
    private float attackCooldown;

    void Start()
    {
        health = 100;
        equipedWeapon = GameObject.Find("Gun").GetComponent<Weapon>();
        equipedWeapon.weapon = weapons[0];
    }

    void Update()
    {
        if (health <= 0) Die();
        equipedWeapon.weapon.GunRotation();

        if (Input.GetKeyDown(KeyCode.Space)) TakeDamage(10);

        ChangeWeapon();
        Shoot();
        Cooldown();
    }

    void Die()
    {
        Destroy(this.gameObject);
        Debug.Log("Me mori");
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
    }

    void ChangeWeapon()
    {
        if (Input.GetKeyDown(KeyCode.X)) {
            indexWeapon--;
            if (indexWeapon < 0) indexWeapon = weapons.Length - 1;
            Debug.Log(weapons[indexWeapon].weaponName);
            equipedWeapon.weapon = weapons[indexWeapon];
            attackCooldown = weapons[indexWeapon].timeBetweenShoots / 2; //Para que no se pueda abusar de cambiar, hago que tenga que esperar un poco entre cambios
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            indexWeapon++;
            if (indexWeapon > weapons.Length - 1) indexWeapon = 0;
            Debug.Log(weapons[indexWeapon].weaponName);
            equipedWeapon.weapon = weapons[indexWeapon];
            attackCooldown = weapons[indexWeapon].timeBetweenShoots / 2; //Para que no se pueda abusar de cambiar, hago que tenga que esperar un poco entre cambios
        }
    }

    void Shoot()
    {
        if (attackCooldown <= 0)
        {
            if (Input.GetMouseButtonDown(0))
            {
                equipedWeapon.weapon.Shoot();
                attackCooldown = equipedWeapon.weapon.timeBetweenShoots;
            }
        } attackCooldown -= Time.deltaTime;
    }

    void Cooldown()
    {
        if (attackCooldown <= 0) ScoreManager.Instance.Cooldown = 0;
        else ScoreManager.Instance.Cooldown = attackCooldown;
    }
}
