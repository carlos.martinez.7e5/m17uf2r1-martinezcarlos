using UnityEngine;

public class PlayerHealthController : MonoBehaviour
{
    [SerializeField] private PlayerDataSOJ _pd;

    private void Start()
    {
        _pd.health = _pd.maxHealth;
    }

    private void Update()
    {
        if (_pd.health <= 0) Destroy(transform.parent.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("EnemyBullet"))
        {
            Destroy(collision.gameObject);
            _pd.health -= collision.GetComponent<BulletBehaviour>().attack;
        }
    }
}
