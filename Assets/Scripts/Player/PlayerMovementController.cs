using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    [SerializeField] private PlayerDataSOJ _pd;

    private Animator _animator;
    private Rigidbody2D _rb;
    
    private Vector3 _movement;
    private float _mX;
    private float _mY;

    private Camera _cam;

    [SerializeField] private float _dashForce;
    [SerializeField] private float _timeBetweenDash;
    [SerializeField] private float _dashCooldown;

    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        _dashCooldown = 0;
    }

    void Update()
    {
        _mX = Input.GetAxis("Horizontal");
        _mY = Input.GetAxis("Vertical");

        //animator.SetFloat("MoveX", mX);
        //animator.SetFloat("MoveY", mY);

        var mousePos = _cam.ScreenToWorldPoint(Input.mousePosition);

        _animator.SetFloat("MoveX", mousePos.x);
        _animator.SetFloat("MoveY", mousePos.y);

        _movement = new Vector3(_mX, _mY,0);
        
        Dash();
    }

    private void FixedUpdate()
    {
        _rb.velocity = _pd.speed * _movement;
    }

    private void Dash()
    {
        if (_dashCooldown <= 0)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Debug.Log("Dash");
                _rb.MovePosition(transform.position + _movement * _dashForce);
                _dashCooldown = _timeBetweenDash;
            }
        } else _dashCooldown -= Time.deltaTime;
    }
}
