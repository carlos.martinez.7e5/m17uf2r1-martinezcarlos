using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillCount : MonoBehaviour
{

    public int enemiesKilled;

    public void CountKillEnemy()
    {
        enemiesKilled++;
    }

}
