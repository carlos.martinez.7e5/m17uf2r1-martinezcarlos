using UnityEngine;

public class PlayerShootController : MonoBehaviour
{
    [SerializeField] PlayerDataSOJ _pd;
    
    private Weapon _equipedWeapon;
    private int _indexWeapon;
    private float _attackCooldown;

    void Start()
    {
        _equipedWeapon = GameObject.Find("Gun").GetComponent<Weapon>();
        _equipedWeapon.weapon = _pd.weapons[0];

        _pd.weapons[0].bullets = 75;
        _pd.weapons[1].bullets = 25;
        _pd.weapons[2].bullets = 5;
    }

    // Update is called once per frame
    void Update()
    {
        _equipedWeapon.weapon.GunRotation();
        ChangeWeapon();
        Shoot();
        Cooldown();
    }

    void ChangeWeapon()
    {
        var _mouseWheel = Input.GetAxisRaw("Mouse ScrollWheel");

        if (Input.GetKeyDown(KeyCode.X) || _mouseWheel > 0)
        {
            _indexWeapon--;
            if (_indexWeapon < 0) _indexWeapon = _pd.weapons.Length - 1;
            Debug.Log(_pd.weapons[_indexWeapon].weaponName);
            _equipedWeapon.weapon = _pd.weapons[_indexWeapon];
            _attackCooldown = _pd.weapons[_indexWeapon].timeBetweenShoots / 2; //Para que no se pueda abusar de cambiar, hago que tenga que esperar un poco entre cambios
        }

        if (Input.GetKeyDown(KeyCode.C) || _mouseWheel < 0)
        {
            _indexWeapon++;
            if (_indexWeapon > _pd.weapons.Length - 1) _indexWeapon = 0;
            Debug.Log(_pd.weapons[_indexWeapon].weaponName);
            _equipedWeapon.weapon = _pd.weapons[_indexWeapon];
            _attackCooldown = _pd.weapons[_indexWeapon].timeBetweenShoots / 2; //Para que no se pueda abusar de cambiar, hago que tenga que esperar un poco entre cambios
        }
    }

    void Shoot()
    {
        if (_attackCooldown <= 0)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (_equipedWeapon.weapon.bullets >= 1)
                {
                    _equipedWeapon.weapon.bullets -= 1;
                    _equipedWeapon.weapon.Shoot();
                    _attackCooldown = _equipedWeapon.weapon.timeBetweenShoots;
                }
            }
        }
        _attackCooldown -= Time.deltaTime;
    }

    void Cooldown()
    {
        if (_attackCooldown <= 0) ScoreManager.Instance.Cooldown = 0;
        else ScoreManager.Instance.Cooldown = _attackCooldown;
    }
}
