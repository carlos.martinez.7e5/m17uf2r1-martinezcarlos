using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script no utilizado
public class DisparoAlternativo : MonoBehaviour
{

    [SerializeField]
    private Transform firePoint;
    [SerializeField]
    private new Camera camera;
    [SerializeField]
    GameObject bullet;
    [SerializeField]
    private float bulletForce;
    private bool canShoot;
    public float attack_speed = 1f;
    public float attack_size = 1f;

    void Start()
    {
        canShoot = true;
    }

    void Update()
    {
        Shoot();
        GunRotation();
    }

    void GunRotation()
    {
        Vector3 mouseWorldPosition = camera.ScreenToWorldPoint(Input.mousePosition);
        mouseWorldPosition.z = 0;

        Vector3 lookAtDirection = mouseWorldPosition - firePoint.position;
        firePoint.right = lookAtDirection;
    }
    void Shoot()
    {
        if (canShoot)
        {
            if (Input.GetMouseButtonDown(0))
            {
                var shoot = Instantiate(bullet, transform.position, Quaternion.identity);
                shoot.transform.localScale *= attack_size;
                shoot.transform.position = firePoint.position;
                shoot.GetComponent<Rigidbody2D>().velocity = firePoint.right * bulletForce;
                StartCoroutine(ShootCooldown());
            }
        }
    }

    private IEnumerator ShootCooldown()
    {
        canShoot = false;
        yield return new WaitForSeconds(attack_speed);
        canShoot = true;
    }
}
