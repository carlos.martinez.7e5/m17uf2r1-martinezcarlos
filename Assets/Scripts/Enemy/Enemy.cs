using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float Life;
    protected SpriteRenderer _sprite;

    [SerializeField] protected int _hitPoints;
    [SerializeField] protected int _deathPoints;

    private void Start()
    {
        _sprite = GetComponent<SpriteRenderer>();
    }

    public virtual void TakeDamage(float damage)
    {
        Life -= damage;
        if (Life <= 0) Death();
        //Debug.Log("Ay! Me diste" + " Me hiciete " + damage + " puntos de da�o");
    }

    public virtual void Death()
    {
        if(GameObject.Find("Player") != null) GameObject.Find("Player").GetComponent<KillCount>().CountKillEnemy();
        ScoreManager.Instance.Score += _deathPoints;
        Destroy(this.gameObject);
    }
}
