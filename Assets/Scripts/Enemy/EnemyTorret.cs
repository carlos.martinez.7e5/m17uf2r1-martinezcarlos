using System.Collections;
using UnityEngine;

public class EnemyTorret : Enemy
{
    private Collider2D target;
    
    [SerializeField] private GameObject _bullet;
    [SerializeField] private float _bulletSpeed;
    [SerializeField] private float _delay;

    [SerializeField] private SpriteRenderer _sr;
    private float _cooldown;

    private float _cooldownBeeingRed;
    private bool _hited;

    void Start()
    {
        if(GameObject.Find("Player") != null) target = GameObject.Find("Player").GetComponent<Collider2D>();
    }

    //�apa para hacer que se vuelva rojo al recibir un disparo sin usar animaciones (no me sale cambiar el color con las propiedades del animator)
    void Hit()
    {
        if (_hited)
        {
            _cooldownBeeingRed = 0.25f;
            _hited = false;
        }

        if (_cooldownBeeingRed > 0)
        {
            _sr.color = Color.red;
            _cooldownBeeingRed -= Time.deltaTime;
        }
        else _sr.color = Color.white;
    }

    void Update()
    {
        LookAtTarget();
        Hit();

        if (_cooldown <= 0) Shoot();
        else _cooldown -= Time.deltaTime;
    }

    void LookAtTarget()
    {
        if(target != null)
        {
            Vector2 direction = target.transform.position - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }

    void Shoot()
    {
        GameObject bala = Instantiate(_bullet, transform.position, transform.rotation);
        bala.GetComponent<Rigidbody2D>().velocity = transform.right * _bulletSpeed;

        _cooldown = _delay;
    }

    //Como el gameObject que tiene el script no es la torreta en s� sino un hijo firepoint, la sobreescribo para que en vez
    //de destruirse el gameObject se destruya el padre.
    public override void Death()
    {
        base.Death();
        Destroy(transform.parent.gameObject);     
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bullet"))
        {
            TakeDamage(collision.GetComponent<BulletBehaviour>().attack);
            _hited = true;
            //Debug.Log("Me quedan " + Life + " puntos de vida");
            ScoreManager.Instance.Score += _hitPoints;
        }
    }
}
