using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipX : MonoBehaviour
{

    private SpriteRenderer _sr;
    private GameObject _target;

    void Start()
    {
        _sr = GetComponent<SpriteRenderer>();
        _target = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (_target != null)
        {
            if (transform.position.x > _target.transform.position.x) _sr.flipX = true;
            else _sr.flipX = false;
        }
    }
}
