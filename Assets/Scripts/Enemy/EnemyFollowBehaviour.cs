using System.Collections;
using UnityEngine;

public class EnemyFollowBehaviour : Enemy
{
    GameObject _target;
    [SerializeField] float _speed;
    [SerializeField] float _explosionDamage;
    [SerializeField] PlayerDataSOJ _pd;

    private Animator _animator;

    private void Start()
    {
        _target = GameObject.Find("Player");
        _animator = GetComponent<Animator>();
    }

    void Update()
    {
        MoveTowardsPlayer();
    }

    void Explode()
    {
        _animator.SetTrigger("Explode");
        _pd.health -= _explosionDamage;
        GameObject.Find("Player").GetComponent<KillCount>().CountKillEnemy();
    }

    //Se llama desde la animación de explotar como evento para que se destruya al terminarla
    public void Destroy()
    {
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bullet"))
        {
            TakeDamage(collision.GetComponent<BulletBehaviour>().attack);
            _animator.SetTrigger("Hit");
            ScoreManager.Instance.Score += _hitPoints;
        }
    }

    private void MoveTowardsPlayer()
    {
        if (_target != null) transform.position = Vector2.MoveTowards(transform.position, _target.transform.position, _speed * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player")) Explode();
        if (collision.collider.CompareTag("EnemyBullet")) Destroy(collision.gameObject);
    }
}
