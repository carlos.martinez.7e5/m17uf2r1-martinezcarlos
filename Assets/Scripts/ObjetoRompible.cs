using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjetoRompible : MonoBehaviour, IDestruible
{
    [SerializeField] float _vida;
    [SerializeField] float _probabilidadSpawn;
    [SerializeField] GameObject[] _itemsSpawn;

    SpriteRenderer _sr;
    
    void Start()
    {
        _sr = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (_vida <= 0) Break();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bullet")) StartCoroutine(TakeDamage(collision.GetComponent<BulletBehaviour>().attack));
    }

    private IEnumerator TakeDamage(float damage)
    {
        _vida -= damage;

        _sr.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        _sr.color = Color.white;
    }

    public void Break()
    { 
        Destroy(this.gameObject);
        Drop();
    }

    public void Drop()
    {
        var _spawnChance = Random.Range(0, 101);

        if (_spawnChance <= _probabilidadSpawn)
        {
            var itemChance = Random.Range(0, _itemsSpawn.Length);
            Instantiate(_itemsSpawn[itemChance], this.transform.position, Quaternion.identity);
        }
    }
}
