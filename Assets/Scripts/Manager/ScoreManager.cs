using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] Text _ammoTxt;
    [SerializeField] Text _scoreTxt;
    [SerializeField] Slider _healthSlider;
    private GameObject _healthBarFill;
    [SerializeField] Text _cooldownTxt;
    [SerializeField] Text _healthTxt;

    [SerializeField] Text _panelScore;

    [SerializeField] PlayerDataSOJ _pd;

    private GameObject _equipedWeapon;
    private KillCount _killCount;

    public int Score;
    public float Cooldown;

    private static ScoreManager _instance;

    [SerializeField] private GameObject panel;

    private LifeState _lifeState;
    private enum LifeState
    {
        high,
        normal,
        low
    }

    public static ScoreManager Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        _instance = this;  
    }

    private void Start()
    {
        _equipedWeapon = GameObject.Find("Gun");
        _killCount = GameObject.Find("Player").GetComponent<KillCount>();
        _lifeState = LifeState.high;
        _healthBarFill = GameObject.Find("Fill");
    }

    void Update()
    {
        LifeColor();
        UpdateUI();
        CheckKilledEnemies();
    }
    void UpdateUI()
    {
        if (Cooldown == 0) _cooldownTxt.text = "0";
        else _cooldownTxt.text = Cooldown.ToString("F1"); //F1 es para el numero de decimales mostrados.

        if (_equipedWeapon != null) _ammoTxt.text = _equipedWeapon.GetComponent<Weapon>().weapon.bullets.ToString();

        _scoreTxt.text = Score.ToString();

        _healthSlider.value = _pd.health;

        if (_pd.health > 0) _healthTxt.text = _pd.health.ToString();
        else _healthTxt.text = "0";
    }

    void LifeColor()
    {
        switch (_pd.health)
        {
            case float n when (n > 60):
                _lifeState = LifeState.high;
                break;
            case float n when (n <= 60 && n > 31):
                _lifeState = LifeState.normal;
                break;
            case float n when (n <= 30):
                _lifeState = LifeState.low;
                break;
        }
        
        switch (_lifeState)
        {
            case LifeState.high:
                _healthBarFill.GetComponent<Image>().color = Color.green;
                break;
            case LifeState.normal:
                _healthBarFill.GetComponent<Image>().color = Color.yellow;
                break;
            case LifeState.low:
                _healthBarFill.GetComponent<Image>().color = Color.red;
                break;
        }
    }

    void CheckKilledEnemies()
    {
        if (_killCount.enemiesKilled >= 15)
        {
            panel.SetActive(true);
            _panelScore.text = "Score: " + Score.ToString();
        }
    }
}
