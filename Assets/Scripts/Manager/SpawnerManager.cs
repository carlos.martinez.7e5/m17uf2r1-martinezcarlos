using UnityEngine;

public class SpawnerManager : MonoBehaviour
{
    [SerializeField] private GameObject _bombs; //Prefab de las bombas
    [SerializeField] private GameObject _torrets; //Prefab de las torretas

    [SerializeField] private int _numberOfEnemiesSpawned; //Numero de enemigos que spawneara cada oleada
    
    [SerializeField] private float _timeBetweenEnemies; //Cada cuanto tiempo spawnearA una oleada
    private float _cooldown;
    [SerializeField] private float _initialCooldown; //El tiempo que pasa hasta que aparece la primera oleada (para no comenzar directamente)

    private bool _canSpawn; 
    [SerializeField] private int _maxEnemies; //El maximo de enemigos que pueden spawnear en la partida
    [SerializeField] private int _enemiesSpawned; //Contador de los enemigos que han spawneado
    
    void Start()
    {
        _canSpawn = true;
        _cooldown = _initialCooldown;
        _enemiesSpawned = 0;
    }

    void Update()
    {
        if (_cooldown <= 0 && _canSpawn) Spawn();
        else _cooldown -= Time.deltaTime;
    }

    void Spawn()
    {
        for(int i = 0; i < _numberOfEnemiesSpawned; i++)
        {
            if (_enemiesSpawned % 3 == 0) Instantiate(_torrets, new Vector3(Random.Range(-3.5f, 4.5f), Random.Range(-2.5f, 3f), 0), Quaternion.identity);
            else Instantiate(_bombs, new Vector3(Random.Range(-3.5f, 4.5f), Random.Range(-2.5f, 3f), 0), Quaternion.identity);
               
            _enemiesSpawned++;
        }

        _cooldown = _timeBetweenEnemies;
        if (_enemiesSpawned >= _maxEnemies) _canSpawn = false;
    }
}
