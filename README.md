Rogue Like prototype Carlos Martínez Pérez

Sentido del juego:

El juego consiste en sobrevivir a una série de oleadas de enemigos. Dispones de tres armas diferentes, cada una
con un daño, cadencia de tiro, y numero de balas distinto. Por el escenario puedes encontrar objetos rompibles
(como cajas o jarrones) que tienen una probabilidad de generar una moneda (que aumenta la puntuación) o un medikit
(que restaura la vida).

Ficha técnica:

- Género: RogueLike, Shooter
- Habilidades de juego: Reflejos, puntería
- Placeres del juego: Desafío, autosuperación
- Tema: Dungeon mediaval
- Estilo: Pixel art, 2D, Chibi
- Secuencia del juego: Acción en tiempo real
- Jugadores: 1 jugador
- Taxonomia: RogueLike 2D con disparos y recolección de items.
- Inmersión del jugador: Ambientación, Gameplay
- Referencias: Enter the Gungeon, The Binding of Isaac

Controles:

- Moverse: WASD o flechas (se recomienda WASD por comodidad)
- Apuntar: Mover el ratón
- Disparar: Click izquierdo
- Cambiar de arma: Rueda del ratón / X - C (se recomienda rueda del ratón por comodidad)
- Dash: Espacio
- Interactuar con item: Acercarse

Observaciones:

- En la esquina inferior izquierda se muestra el numero de balas del arma equipada y el cooldown entre disparos.
He hecho que al cambiar el arma también se tenga un pequeño cooldown para evitar que se abuse del cambio de arma.
(Si puedes disparar automáticamente al cambiar de arma, no tiene sentido el cooldown ya que se podría cambiar y volver a la
anterior sin tener que esperar. Por eso he hecho el cooldown de cambio) 

- El dash tiene un cooldown de 5 segundos

- Las interfaces se han usado para los objetos rompibles, y la herencia de clases para los enemigos

- La maquina de estados se ha usado para el color de la barra de vida, dependiendo de la salud la barra estará de un color más
claro o oscuro.

- El singleton se ha usado para el manager de la interfaz (salud, puntos, balas...)

- Los scriptableObjects se han utilizado para las armas y para los datos del jugador